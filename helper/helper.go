package helper

import "strings"

var Capitalizedvar string = "to share"

func ValidateUserInput(FName string, LName string, email string, userTicket uint, remainingTickets uint) (bool, bool, bool) {
	isValidName := len(FName) > 1 && len(LName) > 1
	isValidEmail := strings.Contains(email, "@")
	isValidTicketNum := userTicket > 0 && userTicket <= remainingTickets
	return isValidName, isValidEmail, isValidTicketNum
}
