package main

import (
	"fmt"
	"golang-booking-app/helper"
	"sync"
	"time"
)

var waitGroup = sync.WaitGroup{}

var conferenceName = "Go Conference"

const conferenceTickets int = 50

var remainingTickets uint = 50

var bookings = make([]UserData, 0)

type UserData struct {
	FName     string
	LName     string
	email     string
	ticketNum uint
}

func main() {

	greetUsers()

	for remainingTickets > 0 {
		FName, LName, email, userTicket := getUserInput()

		isValidName, isValidEmail, isValidTicketNum := helper.ValidateUserInput(FName, LName, email, userTicket, remainingTickets)

		if isValidEmail && isValidName && isValidTicketNum {

			bookTicket(userTicket, FName, LName, email)
			waitGroup.Add(1) // .Add(num_of_thread_spawning) if adding two go task .Add(2)
			go sendTicket(userTicket, FName, LName, email)
			firstNames := getFirstNames()
			fmt.Printf("the first names of bookings are %v\n", firstNames)
			if remainingTickets == 0 {
				fmt.Println("Out conference is booked. please come tomorrow!")
				break
			}
		} else {
			if !isValidEmail {
				fmt.Println("Invalid email id!")
			}
			if !isValidName {
				fmt.Println("Invalid firstname or lastname!")
			}
			if !isValidTicketNum {
				fmt.Println("Invalid number of tickets!")
			}
		}
		fmt.Println(helper.Capitalizedvar)
	}
	waitGroup.Wait() // wait for the threads to complete before exiting main thread
}

func greetUsers() {
	// fmt.Printf("conferenceticket is %T, remaininttickets is %T, conferenceName is %T\n", confTickets, remainTickets, confName)

	fmt.Printf("Welcome to %v booking application!\n", conferenceName)
	fmt.Println("we have total of", conferenceTickets, "tickets and", remainingTickets, "are remaining!")
	fmt.Print("Get your tickets here to attentd!\n\n")
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, item := range bookings {
		firstNames = append(firstNames, item.FName)
	}
	return firstNames
}

func getUserInput() (string, string, string, uint) {
	var FName string
	var LName string
	var email string
	var userTicket uint

	// ask user for their input

	fmt.Println("Enter fname: ")
	fmt.Scan(&FName)

	fmt.Println("Enter lname: ")
	fmt.Scan(&LName)

	fmt.Println("Enter email: ")
	fmt.Scan(&email)

	fmt.Println("Enter number of tickets: ")
	fmt.Scan(&userTicket)
	return FName, LName, email, userTicket
}

func bookTicket(userTicket uint, FName string, LName string, email string) {
	remainingTickets = remainingTickets - userTicket

	// create a map for a user
	var userData = UserData{
		FName:     FName,
		LName:     LName,
		email:     email,
		ticketNum: userTicket,
	}
	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)
	fmt.Printf("Thank you %v %v for booking %v tikets. You will receive a confirmation email at %v\n", FName, LName, userTicket, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)
}

func sendTicket(userTickets uint, FName string, LName string, email string) {
	fmt.Println("########")
	fmt.Println("generating ticket")
	time.Sleep(10 * time.Second)
	fmt.Println("ticket generated")
	var ticket = fmt.Sprintf("%v tickets for %v %v\n", userTickets, FName, LName)
	fmt.Printf("sending ticket:\n%vto email %v\n", ticket, email)
	time.Sleep(2 * time.Second)
	fmt.Println("email sent successfully!")
	fmt.Println("########")
	waitGroup.Done() // i am done main thread needs not to wait
}
